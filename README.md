# Validador XML
[![pipeline status](https://gitlab.com/Tritonar/video_club/badges/master/pipeline.svg)](https://gitlab.com/Tritonar/video_club/-/commits/master)
[![coverage report](https://gitlab.com/Tritonar/video_club/badges/master/coverage.svg)](https://gitlab.com/Tritonar/video_club/-/commits/master)


# Programación Distribuida II - TP1 

Objetivo

El objetivo es ir conociendo algunas herramientas y visitar algunos conocimientos ya adquiridos.  Para esto construiremos una pequeña API REST.
- El sistema recibe un xml.(object_id,client_id,status,fecha)
- Validar object_idy client_id tengan formato de UUID.
- El formato de la fecha en AAAA/MM/DD.
- Validar el estado (RENT/ DELIVERY_TO_RENT / RETURN / DELIVERY_TO_RETURN).
- Integrado a un servidor de integración continua.

#### Instalación
Se detallan los pasos de para probar el código usando un entorno virtual. Si el sistema ya tiene esto omitir este paso.
```sh
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update && sudo apt upgrade
sudo apt install python3.6 python3.6-dev python3.6-venv
```
Creamos una carpeta para el proyecto. 

```sh
mkdir app_video && cd app_video
```
#### Configuramos el entorno virtual con python 3.6

```sh
python3.6 -m venv venv_app
```

Activamos el entorno.

```sh
source venv_app/bin/activate
```
Clonamos el repositorio
```sh
git clone https://gitlab.com/inginformaticaundav/pd2/2c2020/video_club && cd video_club
```
Use el administrador de paquetes pip para instalar las dependencias.

```sh
pip install -r requirements.txt
```
Migramos la base de datos.

```sh
python manage.py makemigrations rent && python manage.py migrate

```

#### Para iniciar el servidor

```sh
python manage.py runserver
```
Si al iniciar el servidor muestra alguna advertencia o problemas con las base de datos puede correr el script para limpiar la base y generar las tablas. 

```sh
bash build.sh
```

El servidor se puede acceder en http://localhost:8000/

- Para usar la API-REST en formato xml 
    - [http://localhost:8000/rent/](http://localhost:8000/rent/)

![alt text](img/xml.png "GET xml")


#### Para probar la API utilizamos postman
[Postman](https://www.postman.com/) es una herramienta que permite crear peticiones sobre APIs de forma sencilla.

- Instrucciones de uso 
    - 1. Desplegamos las opciones y seleccionamos la opción POST
    - 2. Seleccionar Body.
    - 3. La pestaña raw para ingresar el texto.
    - 4. Seleccionar la opción de formato XML
![alt text](img/postman_.png "POSTMAN")

#### Para validar un xml.
- Configurar con las opciones para realizar un POST usando la url [Rent](http://localhost:8000/rent/)

    - Introducir en el cuerpo el formato adecuado para el alquiler (RENT)
        ```sh
        <rent>
            <object_id>a103456c-5914-4132-bd68-68a04a552017</object_id>
            <client_id>1207a63c-1a25-0278-ef66-01a87be66176</client_id>
            <details>
                <status>RETURN</status>
                <until>2003/11/02</until>
            </details>
        </rent>
        ```
    - Presionar Send.

![alt text](img/post_201.png "POST cliente xml")
#### Para correr los test utilice.  
```sh
python manage.py test
```
```sh
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
........
-------------------------------------------------------
Ran 8 tests in 0.050s

OK
Destroying test database for alias 'default'...

```

#### Coverage report
Utilizamos coverage para correr los test y generar un reporte. 

```sh
coverage run --source='.' manage.py test
```
Para visualizar el reporte 
```sh
coverage report
```
```sh
Name                              Stmts   Miss  Cover
-----------------------------------------------------
club/__init__.py                      0      0   100%
club/settings.py                     19      0   100%
club/urls.py                          3      0   100%
club/wsgi.py                          4      4     0%
manage.py                            12      2    83%
rent/__init__.py                      0      0   100%
rent/admin.py                         2      0   100%
rent/apps.py                          3      0   100%
rent/migrations/0001_initial.py       6      0   100%
rent/migrations/__init__.py           0      0   100%
rent/models.py                       23      0   100%
rent/serializers.py                   7      0   100%
rent/tests.py                        35      0   100%
rent/urls.py                          6      0   100%
rent/views.py                        10      0   100%
-----------------------------------------------------
TOTAL                               130      6    95%

```


