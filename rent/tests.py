from django.test import TestCase
from django.urls import reverse
from rent.views import *
from rest_framework.test import RequestsClient
#-----------------------------------

# Create your tests here.

class UrlTests(TestCase):

    def test_get_rent_url(self):
        # get correct url data
        response = self.client.get('http://127.0.0.1:8000/rent/')
        assert response.status_code == 200

    def test_get_rent_bad_url(self):
        # get data with invalid url
        response = self.client.get('http://127.0.0.1:8000/rents/')
        assert response.status_code == 404

    def test_rent_post(self):
        # correct format validation test of all objects
        data = """<?xml version="1.0" encoding="UTF-8"?>
            <rent>
                <object_id>a123456c-5914-4132-bd68-68a04a552017</object_id>
                    <client_id>b172a2ab-5913-4032-bd68-69a041752017</client_id> 
                    <details>
                        <status>RETURN</status>   
                            <until>2014/12/25</until>
                    </details>
             </rent>"""
        response = self.client.post('http://127.0.0.1:8000/rent/',data=data,content_type='application/xml')
        assert response.status_code == 201

    def test_invalid_date_post(self):
        # test date YYYY/mm/dd , bad format 2013-09-08
        data = """<?xml version="1.0" encoding="UTF-8"?>
            <rent>
                <object_id>a123456c-5914-4132-bd68-68a04a552017</object_id>
                    <client_id>b172a2ab-5913-4032-bd68-69a041752017</client_id>
                    <details>
                        <status>RETURN</status>   
                            <until>2013-09-08</until>
                    </details>
             </rent>"""
        response = self.client.post('http://127.0.0.1:8000/rent/',data=data,content_type='application/xml')
        assert response.status_code == 400

    def test_invalid_date_1_post(self):
        # test date YYYY/mm/dd , bad format 2013-09-08
        data = """<?xml version="1.0" encoding="UTF-8"?>
            <rent>
                <object_id>a123456c-5914-4132-bd68-68a04a552017</object_id>
                    <client_id>b172a2ab-5913-4032-bd68-69a041752017</client_id> 
                    <details>
                        <status>RETURN</status>   
                            <until>09-13-2014</until>
                    </details>
             </rent>"""
        response = self.client.post('http://127.0.0.1:8000/rent/',data=data,content_type='application/xml')
        assert response.status_code == 400

    def test_invalid_status_post(self):
        #test status  'RENT', 'DELIVERY_TO_RENT', 'RETURN','DELIVERY_TO_RETURN', bad fotmat RETURN
        data = """<?xml version="1.0" encoding="UTF-8"?>
            <rent>
                <object_id>a123456c-5914-4132-bd68-68a04a552017</object_id>
                    <client_id>b172a2ab-5913-4032-bd68-69a041752017</client_id> 
                    <details>
                        <status>RETURNET</status>   
                            <until>2019/10/21</until>
                    </details>
             </rent>"""
        response = self.client.post('http://127.0.0.1:8000/rent/',data=data,content_type='application/xml')
        assert response.status_code == 400

    def test_invalid_object_id_post(self):
        #test UUID version 4, bad fotmat a12P456c-5914-4132-bd68-68a04a552017...
        data = """<?xml version="1.0" encoding="UTF-8"?>
            <rent>
                <object_id>a12P456c-5914-4132-bd68-68a04a552017</object_id>
                    <client_id>b172a2ab-5913-4032-bd68-69a041752017</client_id>
                    <details>
                        <status>RETURN</status>   
                            <until>2012/07/20</until>
                    </details>
             </rent>"""
        response = self.client.post('http://127.0.0.1:8000/rent/',data=data,content_type='application/xml')
        assert response.status_code == 400

    def test_invalid_client_id_post(self):
        # invalid UUID version 4, bad fotmat b1X-a2ab-5913-4032-bd68-69a041752017...
        data = """<?xml version="1.0" encoding="UTF-8"?>
            <rent>
                <object_id>a127456c-5914-4132-bd68-68a04a552017</object_id>
                    <client_id>b1X-a2ab-5913-4032-bd68-69a041752017</client_id>
                    <details>
                        <status>RETURN</status>   
                            <until>2009/02/14</until>
                    </details>
             </rent>"""
        response = self.client.post('http://127.0.0.1:8000/rent/',data=data,content_type='application/xml')
        assert response.status_code == 400






