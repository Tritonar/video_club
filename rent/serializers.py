from rest_framework import serializers
from rest_framework_json_api.relations import ResourceRelatedField
from .models import *

class FilmSerializer(serializers.ModelSerializer):
    """
    clase para serializar el modelo Film
    @version 1.0.0__
    """
    class Meta:
        model = Film
        fields =  ['status','until']


class RentsSerializer(serializers.ModelSerializer):
    """
    clase para serializar el modelo Rent
    @version 1.0.0__
    """
    details = FilmSerializer()
    class Meta:
        model = Rents
        fields = ['object_id','client_id','details']
    
    def create(self, validated_data):
        profile_data = validated_data.pop('details')
        film_ob = Film.objects.create(**profile_data)
        return Rents.objects.create(details=film_ob, **validated_data)

