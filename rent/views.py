from django.shortcuts import render
from rest_framework import viewsets 
from .models import *
from .serializers import *
from .renderer import *
from rest_framework_xml.parsers import XMLParser
from rest_framework_xml.renderers import XMLRenderer


class RentsViewSet(viewsets.ModelViewSet):
    """
    @Vistas de Alquiler
    @date 23-10-2020
    @version 1.0.0
    """
    model = Rents
    queryset = Rents.objects.all()
    serializer_class = RentsSerializer
    renderer_classes = (RentsXMLRenderer,)
