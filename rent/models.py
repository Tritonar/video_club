from django.db import models
from django.db.models import Model
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _
from datetime import datetime
from uuid import UUID


def validate_uuid(value) :
    try:
        uuid_obj = UUID(value, version=4)
    except ValueError:
        raise ValidationError(
            _('%(value)s , is not uuid version=4  =['),
            params={'value': value},        )
def validate_date(value):
    try:
        date_object = datetime.strptime(str(value), "%Y/%m/%d")
    except ValueError:
        raise ValidationError(
                _('%(value)s , is not  date valid YYYY/MM/DD =['),
                params={'value': value},            )

class Film(models.Model):
    STATUS_ = (('RENT', _('RENT')),('DELIVERY_TO_RENT', _('DELIVERY_TO_RENT')),
        ('RETURN', _('RETURN')),('DELIVERY_TO_RETURN', _('DELIVERY_TO_RETURN')),)
    status = models.CharField (choices=STATUS_, max_length = 20)
    until=models.CharField(validators=[validate_date], max_length = 32)

class Rents(models.Model):
    id_rents=models.AutoField(primary_key=True)
    object_id= models.CharField( validators=[validate_uuid], max_length = 120)
    client_id = models.CharField( validators=[validate_uuid], max_length = 120)
    details= models.ForeignKey(Film, on_delete=models.CASCADE)